package org.qwerty.cpc

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.qwerty.cpc.adapters.FileListAdapter
import org.qwerty.cpc.dialogs.BluetoothSettingsDialog
import java.io.File
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {

    private val REQUEST_ENABLE_BT = 1
    private val AUTO_CONNECTION = "auto_connect"
    private lateinit var menu: Menu

    var fileListAdapter = FileListAdapter(supportFragmentManager)
    lateinit var socket: BluetoothSocket
    var autoConnection = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        download_button.setOnClickListener {
            if (::socket.isInitialized)
                downloadFile()
            else
                Toast.makeText(this, "Bluetooth устройсто не подключено!", Toast.LENGTH_LONG).show()
        }

        showFiles()
        initBluetooth()
        initAutoConnectionValue()
        hideDownloadElements()
    }

    override fun onStop() {
        if (::socket.isInitialized) {
            var preferences = getPreferences(Context.MODE_PRIVATE)
            var editor = preferences.edit()
            editor.putBoolean(AUTO_CONNECTION, autoConnection)
            editor.commit()
        }
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        this.menu = menu!!
        return true
    }

    fun updateBluetoothIcon(drawable: Int) {
        var item = menu.findItem(R.id.bluetooth_status)
        item.setIcon(drawable)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var dialog = BluetoothSettingsDialog()
        dialog.show(supportFragmentManager, "dialog")
        return super.onOptionsItemSelected(item)
    }


    private fun initBluetooth() {
        var bluetooth = BluetoothAdapter.getDefaultAdapter()
        if (bluetooth == null) {
            Toast.makeText(
                this,
                "На вашем устройстве отсутствует Bluetooth адаптер!",
                Toast.LENGTH_LONG
            ).show()
            finish()
        }
        if (!bluetooth!!.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_ENABLE_BT ->
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(this, "Необходимо включить bluetooth", Toast.LENGTH_LONG).show()
                    initBluetooth()
                }
        }
    }

    fun isSocketInitialized() = ::socket.isInitialized

    private fun generateFileName(): String {
        return "${System.currentTimeMillis()}.dat";
    }

    private fun downloadFile() {
        var dir = getDir("data", Context.MODE_PRIVATE)
        var file = FileOutputStream(File(dir, generateFileName()))
        var thread = DataThread(this, file.bufferedWriter(), socket)
        thread.start()
    }

    fun updateFiles() {
        fileListAdapter.clear()
        var dir = getDir("data", Context.MODE_PRIVATE)
        for (file in dir.listFiles()) {
            fileListAdapter.add(file)
        }
    }

    private fun showFiles() {
        files_view.layoutManager = LinearLayoutManager(this)
        files_view.adapter = fileListAdapter

        var dir = getDir("data", Context.MODE_PRIVATE)
        for (file in dir.listFiles()) {
            fileListAdapter.add(file)
        }
    }

    private fun initAutoConnectionValue() {
        var preferences = getPreferences(Context.MODE_PRIVATE)
        autoConnection = preferences.getBoolean(AUTO_CONNECTION, false)
    }

    fun hideDownloadElements() {
        download_layout.visibility = View.GONE
        download_button.isEnabled = true
    }

    //todo добавить параметр, максимальное значение прогресс бара
    fun showDownloadElements() {
        download_layout.visibility = View.VISIBLE
        download_bar.progress = 0
        download_button.isEnabled = false
    }

    fun updateProgress(progress: Int) {
        download_bar.progress = progress
    }
}
