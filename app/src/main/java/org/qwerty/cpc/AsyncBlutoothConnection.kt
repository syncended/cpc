package org.qwerty.cpc

import android.bluetooth.BluetoothSocket
import android.os.AsyncTask
import android.widget.Toast
import org.qwerty.cpc.dialogs.BluetoothConnectionDialog

class AsyncBlutoothConnection(parent: MainActivity) : AsyncTask<BluetoothSocket, Void, Boolean>() {
    private var parent = parent
    private lateinit var dialog: BluetoothConnectionDialog

    override fun onPreExecute() {
        super.onPreExecute()
        dialog = BluetoothConnectionDialog()
        dialog.show(parent.supportFragmentManager, "connectionDialog")

    }

    override fun doInBackground(vararg socket: BluetoothSocket?): Boolean? {
        try {
            socket[0]!!.connect()
            parent.socket = socket[0]!!
            return true
        } catch (ex: Exception) {
            return false
        }
    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        dialog.dismiss()
        if (!result!!)
            Toast.makeText(parent, "Соединение не удалось!", Toast.LENGTH_LONG).show()
        else
            parent.updateBluetoothIcon(R.drawable.baseline_bluetooth_connected_24)

    }
}