package org.qwerty.cpc

import android.bluetooth.BluetoothSocket
import android.util.Log
import android.widget.Toast
import java.io.BufferedWriter
import java.io.OutputStream
import java.io.OutputStreamWriter

class DataThread(activity: MainActivity, outputFile: BufferedWriter, socket: BluetoothSocket) : Thread() {
    private val INPUT_DATA_SIZE = 43
    private var activity = activity
    private var socket = socket
    private var outputFile = outputFile
    override fun run() {
        activity.runOnUiThread {
            activity.showDownloadElements()
        }
        try {
            var output = socket.outputStream
            output.write("begin".toByteArray())
            output.flush()
            var input = socket.inputStream
            var message: String
            //tst
            var i = 1
            while (true) {
                var data = ByteArray(INPUT_DATA_SIZE)
                input.read(data)
                message = String(data)

                if (message.substring(0..2) == "end")
                    break

                //tst
                activity.runOnUiThread {
                    activity.updateProgress(i)
                }
                //tst
                i++
                outputFile.write(message)
                outputFile.newLine()
                outputFile.flush()
            }
            outputFile.close()
            activity.runOnUiThread {
                activity.hideDownloadElements()
                Toast.makeText(activity, "Загрузка завершена!", Toast.LENGTH_LONG).show()
                activity.updateFiles()
            }
        } catch (ex: Exception) {
            Log.e(this::class.java.name, ex.message)
        }

    }
}