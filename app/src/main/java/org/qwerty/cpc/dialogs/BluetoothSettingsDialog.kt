package org.qwerty.cpc.dialogs

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import org.qwerty.cpc.adapters.BluetoothListAdapter
import org.qwerty.cpc.AsyncBlutoothConnection
import org.qwerty.cpc.MainActivity
import org.qwerty.cpc.R
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

class BluetoothSettingsDialog : DialogFragment() {

    private val DEVICE_NAME = "em"

    private lateinit var parent: MainActivity

    private var receiver = BluetoothBroadcastReceiver()
    private var bluetooth = BluetoothAdapter.getDefaultAdapter()
    private var handler = Handler()

    private lateinit var newConnectionsAdapter: BluetoothListAdapter
    private lateinit var usedDevicesAdapter: BluetoothListAdapter
    private var usedDevices = HashSet<String>()

    private lateinit var scanButton: Button
    private lateinit var scanBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
        loadUsedDevices()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.bluetooth_settings_dialog, container, false)

        parent = activity as MainActivity

        var autoConnection: SwitchMaterial = view.findViewById(R.id.auto_connection_switch)
        autoConnection.isChecked = parent.autoConnection
        autoConnection.setOnCheckedChangeListener { compoundButton, isChecked ->
            parent.autoConnection = isChecked
        }
        var usedDevicesView = view.findViewById<RecyclerView>(R.id.previous_connections)
        var newDevicesView = view.findViewById<RecyclerView>(R.id.new_connections)

        usedDevicesAdapter = BluetoothListAdapter(this)
        usedDevicesView.layoutManager = LinearLayoutManager(context)
        usedDevicesView.adapter = usedDevicesAdapter

        newConnectionsAdapter = BluetoothListAdapter(this)
        newDevicesView.adapter = newConnectionsAdapter
        newDevicesView.layoutManager = LinearLayoutManager(context)

        scanBar = view.findViewById(R.id.scan_bar)

        scanButton = view.findViewById(R.id.scan_button)
        scanButton.setOnClickListener {
            if (bluetooth.isDiscovering)
                cancelScanDevices()
            else
                scanDevices()

        }

        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        context!!.registerReceiver(receiver, filter)

        scanDevices()

        return view
    }

    override fun onDestroyView() {
        cancelScanDevices()
        saveUsedDevices()
        context!!.unregisterReceiver(receiver)
        super.onDestroyView()
    }

    // Сканирование устройств
    private fun scanDevices() {
        val bluetooth = BluetoothAdapter.getDefaultAdapter()

        handler.postDelayed({
            cancelScanDevices()
        }, 30000)

        scanButton.text = "закончить сканирование"
        scanBar.visibility = View.VISIBLE

        newConnectionsAdapter.clear()
        usedDevicesAdapter.clear()

        bluetooth.startDiscovery()
    }

    private fun cancelScanDevices() {
        handler.removeCallbacksAndMessages(null)
        bluetooth.cancelDiscovery()
        scanButton.text = "поиск устройств"
        scanBar.visibility = View.GONE
    }

    // Добавляем подключенное устройство в ранее использованые
    fun addUsedDevice(device: BluetoothDevice) {
        usedDevices.add(device.address)
        dismiss()
    }

    /**
     * Метод записывает объект usedDevices в файл
     * при помощи java serialization
     */
    private fun saveUsedDevices() {
        try {
            var outputStream =
                ObjectOutputStream(context!!.openFileOutput("usedDevices", Context.MODE_PRIVATE))
            outputStream.writeObject(usedDevices)
            outputStream.flush()
            outputStream.close()
        } catch (ex: Exception) {
            Log.e("cpc", ex.message)
        }
    }

    /**
     * Загружает объект usedDevices из файла
     */
    private fun loadUsedDevices() {
        try {
            var inputStream = ObjectInputStream(context!!.openFileInput("usedDevices"))
            usedDevices = inputStream.readObject() as HashSet<String>
            inputStream.close()
        } catch (ex: Exception) {
            Log.e("cpc", ex.message)
        }
    }

    fun connect(device: BluetoothDevice) {
        /**
         * Обычный метод
         * device.createRfcommSocketToServiceRecord()
         * не работает правильно, поэтому приходится использовать
         * данную конструкуию для создания BluetoothSocket
         */
        var socket = device.javaClass.getMethod(
            "createRfcommSocket",
            *arrayOf<Class<*>>(Int::class.javaPrimitiveType!!)
        ).invoke(device, 1) as BluetoothSocket
        var task = AsyncBlutoothConnection(parent)

        task.execute(socket)
        addUsedDevice(device)
    }

    inner class BluetoothBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            if (BluetoothDevice.ACTION_FOUND == intent!!.action) {
                var device =
                    intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                Log.i(this::class.java.name, "Device ${device.name} found!")
                if (device.name != null && device.name.startsWith(DEVICE_NAME)) {
                    when (usedDevices.contains(device.address)) {
                        true -> {
                            if (!parent.isSocketInitialized() && parent.autoConnection)
                                connect(device)

                            usedDevicesAdapter.add(device)
                        }
                        false ->
                            newConnectionsAdapter.add(device)
                    }
                }
            }
        }
    }
}
