package org.qwerty.cpc.dialogs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.MaterialToolbar
import org.qwerty.cpc.adapters.FileViewAdapter
import org.qwerty.cpc.R
import java.io.File
import java.lang.Exception

class FileViewDialog(file: File) : DialogFragment(){

    private var file = file
    private var fileViewAdapter = FileViewAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.file_view_dialog, container, false)

        var toolbar = view.findViewById<MaterialToolbar>(R.id.file_name_toolbar)
        toolbar.title = file.name

        var fileView: RecyclerView = view.findViewById(R.id.file_view)
        fileView.layoutManager = LinearLayoutManager(context)
        fileView.adapter = fileViewAdapter

        loadFile()
        return view
    }

    private fun loadFile() {
        try {
            file.forEachLine {
                fileViewAdapter.add(it)
            }
        }catch (ex: Exception) {
            Toast.makeText(context, ex.message, Toast.LENGTH_LONG).show()
            Log.e(this::class.java.name, ex.message)
            dismiss()
        }
    }
}