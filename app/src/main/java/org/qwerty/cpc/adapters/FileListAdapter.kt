package org.qwerty.cpc.adapters

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import org.qwerty.cpc.MainActivity
import org.qwerty.cpc.dialogs.FileViewDialog
import org.qwerty.cpc.R
import java.io.File

class FileListAdapter(fragmentManager: FragmentManager) :
    RecyclerView.Adapter<FileListAdapter.ViewHolder>() {
    private var dataSet = ArrayList<File>()
    private var fragmentManager = fragmentManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.file_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fileName.text = dataSet[position].name
    }

    fun add(file: File) {
        dataSet.add(file)
        notifyDataSetChanged()
    }

    fun clear() {
        dataSet.clear()
        notifyDataSetChanged()
    }

    private fun deleteFile(position: Int) {
        dataSet[position].delete()
        dataSet.removeAt(position)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var fileName: TextView = view.findViewById(R.id.file_name)

        init {
            view.setOnClickListener {
                var dialog = FileViewDialog(dataSet[adapterPosition])
                dialog.show(fragmentManager, "dialog")

            }
            view.setOnLongClickListener {
                var popup = PopupMenu(view.context, view)
                popup.inflate(R.menu.file_menu)

                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {

                        R.id.menu_delete -> {
                            deleteFile(adapterPosition)
                        }
                    }
                    true
                }
                popup.show()
                true
            }

        }
    }

}