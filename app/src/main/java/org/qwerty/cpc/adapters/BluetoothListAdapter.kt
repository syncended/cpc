package org.qwerty.cpc.adapters

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.qwerty.cpc.dialogs.BluetoothSettingsDialog
import org.qwerty.cpc.MainActivity
import org.qwerty.cpc.R


class BluetoothListAdapter(val parent: BluetoothSettingsDialog) :
    RecyclerView.Adapter<BluetoothListAdapter.ViewHolder>() {

    private var dataSet = ArrayList<BluetoothDevice>()

    override fun getItemCount() = dataSet.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.bluetooth_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bluetoothName.text = "${dataSet[position].name} ${dataSet[position].address}"
        var activity = parent.activity as MainActivity
        if(activity.isSocketInitialized() && activity.socket.remoteDevice!!.address == dataSet[position].address)
            holder.connectedView.visibility = View.VISIBLE

    }

    fun add(device: BluetoothDevice) {
        if (!dataSet.contains(device)) {
            dataSet.add(device)
            notifyDataSetChanged()
        }
    }

    fun clear() {
        dataSet.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var bluetoothName: TextView = view.findViewById(R.id.bluetooth_name)
        var connectedView: ImageView =  view.findViewById(R.id.connected_view)

        init {
            view.setOnClickListener {
                parent.connect(dataSet[adapterPosition])
            }
        }
    }

}